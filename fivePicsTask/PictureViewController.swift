//
//  PictureViewController.swift
//  fivePicsTask
//
//  Created by V.K. on 2/20/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit

class PictureViewController: UIViewController {
    
    var imageName: String = "noimage"
    
    @IBOutlet weak var imagePlace: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePlace.image = UIImage(named: imageName)
    }

}
