//
//  ViewController.swift
//  fivePicsTask
//
//  Created by V.K. on 2/20/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let pictures: [Int:String] = [
        1: "orangeFruit",
        2: "appleRed",
        3: "DragonFruit",
        4: "Kiwi",
        5: "Pomegranate"
    ]
    
   @IBAction func imageSelection(_ sender: UIButton) {
        
        guard let pictureVC = storyboard?.instantiateViewController(identifier: "PictureViewController") as? PictureViewController,
            let imageToShow = pictures[sender.tag]
        else {
            return
        }
                
        pictureVC.imageName = imageToShow
        navigationController?.pushViewController(pictureVC, animated: true)
    
    }
    
}

